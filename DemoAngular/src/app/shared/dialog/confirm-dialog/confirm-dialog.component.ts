import { Component, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogModel } from '../../../models/dialog.model';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent {
  // define which main color of submit button
  public color: string;
  public isHtmlTitle: boolean;
  public isHtmlBody: boolean;


  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogModel
  ) {
    this.color = data.color || 'primary';
    this.isHtmlTitle = data.isHtmlTitle || false;
    this.isHtmlBody = data.isHtmlBody || false;

  }
}
