import { DialogModel } from '../../../models/dialog.model';

export const CONFIRM_DIALOG_CONFIG: DialogModel = {
    title: '',
    body: '',
    confirmButton: 'Delete',
    cancelButton: 'Cancel',
    color: 'error',
    isHtmlBody: false,
    isHtmlTitle: false
};
