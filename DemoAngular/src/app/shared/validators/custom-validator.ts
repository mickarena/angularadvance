import { Injectable } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, AbstractControl, ValidationErrors  } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})

export class CustomValidator {
  static noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    // tslint:disable-next-line:object-literal-key-quotes
    return isValid ? null : { 'whitespace': true };
  }


  static compareControl(
    fromControlName: string,
    toControlName: string,
    fnCompare: any,
  ) {
    return (formGroup: FormGroup) => {
      const fromControl = formGroup.controls[fromControlName];
      const toControl = formGroup.controls[toControlName];

      if (fnCompare(fromControl.value, toControl.value)) {
        fromControl.setErrors({ incorrectDate: true });
      } else {
        fromControl.setErrors(null);
      }
    };
  }


  public integer(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const error: ValidationErrors = { integer: true };
      if (control.value && control.value !== `${parseInt(control.value, 10)}`) {
        control.setErrors(error);
        return error;
      }
      control.setErrors(null);
      return null;
    };
  }

    public isInteger = (control: FormControl) => {
        return this.check_if_is_integer(control.value) ? null : {
          notNumeric: true
        };
    }

    public check_if_is_integer(value) {
      // tslint:disable-next-line: radix
      return ((parseFloat(value) == parseInt(value)) && !isNaN(value));
    }

}
