import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

export class AppCustomDirective extends Validators {

    static timeValidator(formGroupValues: FormGroup): any {
        if (!formGroupValues.value.begin && !formGroupValues.value.end) {
            return { required: true};
        }
    }
}

