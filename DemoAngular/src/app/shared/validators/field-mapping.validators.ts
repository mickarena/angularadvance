import {AbstractControl, FormGroup} from '@angular/forms';

export function onlyAlphabets(c: AbstractControl) {
  const value = c.value;
  if (!value) {
    return null;
  }
  return /^[a-z]*$/i.test(value) ? null : {
    onlyAlphabetsRule: true
  };
}

export function checkDuplicateColumn(fg: FormGroup) {
  const formValue = fg.value as {
    [key: string]: string;
  };
  const controls = Object.entries(formValue);
  const map: {
    [key: string]: string[];
  } = {};

  let hasDuplicated = false;
  for (const control of controls) {
    const key = control[0];
    const value = control[1];
    if (value) {
      if (map[value]) {
        hasDuplicated = true;
        map[value].push(key);
      } else {
        map[value] = [key];
      }
    }
  }
  if (hasDuplicated) {
    const duplicateControls: {
      [key: string]: string[];
    } = {};
    Object.entries(map).forEach(([key, values]) => {
      if (values.length > 1) {
        duplicateControls[key] = values;
      }
    });
    return {
      duplicateControls
    };
  }
  return null;
}
