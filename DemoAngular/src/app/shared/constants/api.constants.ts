export const LOG_LEAVE_API_GET    = '/api/LogLeave/Get';
export const LOG_LEAVE_API_GETAll = '/api/LogLeave/GetAll';
export const LOG_LEAVE_API_CREATE = '/api/LogLeave/Create';
export const LOG_LEAVE_API_UPDATE = '/api/LogLeave/Update';
export const LOG_LEAVE_API_DELETE = '/api/LogLeave/Delete';
export const LOG_LEAVE_API_APPROVEL = '/api/LogLeave/Approvel';
export const LOG_LEAVE_API_REJECT = '/api/LogLeave/Reject';


export const MANAGE_USER_API_GETUSERADMIN = 'api/ManageUser/GetUserAdmin';
export const MANAGE_USER_API_GET = '/api/ManageUser/Get';
export const MANAGE_USER_API_CREATE = '/api/ManageUser/Create';
export const MANAGE_USER_API_UPDATE = '/api/ManageUser/Update';
export const MANAGE_USER_API_DELETE = '/api/ManageUser/Delete';
