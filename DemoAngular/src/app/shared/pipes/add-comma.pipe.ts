import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addComma'
})

export class AddCommaPipe implements PipeTransform {
  transform(value, keepTrailingZero: boolean = false) {
    return this.pipeValue(value, keepTrailingZero);
  }

  pipeValue(value, keepTrailingZero) {
    if (!value && value !== 0) {
      return;
    }
    let negative = '';
    if (value.toString().startsWith('-')) {
      negative = '-';
      value = value.toString().substring(1);
    }
    value = value.toString().replace(/,/g, '');
    const splitItems = value.split('.');
    const arr: string[] = [];
    for (let i = splitItems[0].length - 3; i > 0; i = i - 3) {
      arr.push(',' + splitItems[0].substr(i, 3));
      splitItems[0] = splitItems[0].substring(0, i);
    }
    arr.reverse();
    arr.forEach((item: string) => {
      splitItems[0] = splitItems[0] + item;
    });
    if (splitItems[1]) {
      if (!keepTrailingZero) {
        const lengthDecimal = splitItems[1].length;
        for (let iDecimal = 0; iDecimal < lengthDecimal; iDecimal++) {
          if (+splitItems[1][splitItems[1].length - 1] === 0) {
            splitItems[1] = splitItems[1].substring(0, splitItems[1].length - 1);
          } else {
            break;
          }
        }
      }
      if (+splitItems[1] === 0) {
        value = splitItems[0];
      } else {
        if (splitItems[1].toString().substring(1) === '.') {
          value = splitItems[0];
        } else {
          value = `${splitItems[0]}.${splitItems[1]}`;
        }
      }
    } else {
      value = splitItems[0];
    }

    return negative + value;
  }
}
