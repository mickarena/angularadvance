import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortNumber'
})

export class ShortNumberPipe implements PipeTransform {
  public trailingZeroRegex = /0+$/g;
  public levelMapper = [
    { name: 'K', rounder: Math.pow(10, 3) },
    { name: 'M', rounder: Math.pow(10, 6) },
    { name: 'B', rounder: Math.pow(10, 9) },
    { name: 't', rounder: Math.pow(10, 12) },
  ];

  transform(num, args?: any) {
    if (num === null) {
      return null;
    }

    if (num === '0' || num === 0) {
      return '0';
    }

    if (+num > 0 && +num < 1) {
      const split = parseFloat(num).toString().split('.');
      const result = Math.ceil(+split[1] / 10);
      const division = (result / Math.pow(10, result.toString().length)).toString();
      return parseFloat(division).toFixed(1);
    }

    if (+num > 0 && +num < 999.95) {
      if (num.toString().includes('.')) {
        const v = parseFloat(num.toString()).toFixed(1);
        return v.toString();
      } else {
        return num.toString();
      }
    }

    let abs: any;
    abs = Math.abs(+num);

    for (const level of this.levelMapper) {
      const devidedValue = abs / level.rounder;
      const condition = devidedValue > Math.pow(10, 3);
      if (condition) {
        continue;
      } else {
        return `${parseFloat(devidedValue.toString()).toFixed(1)}${level.name}`;
      }

    }
  }
}
