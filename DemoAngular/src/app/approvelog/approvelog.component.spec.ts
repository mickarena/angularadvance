import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovelogComponent } from './approvelog.component';

describe('ApprovelogComponent', () => {
  let component: ApprovelogComponent;
  let fixture: ComponentFixture<ApprovelogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprovelogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovelogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
