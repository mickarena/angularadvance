import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmApprovelComponent } from './confirm-approvel.component';

describe('ConfirmApprovelComponent', () => {
  let component: ConfirmApprovelComponent;
  let fixture: ComponentFixture<ConfirmApprovelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmApprovelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmApprovelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
