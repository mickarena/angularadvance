import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { takeWhile } from 'rxjs/operators';
import { LogLeaveService } from '../../services/logleave.service';
import { TypeLogLeave } from '../../models/type-log-leave.mode';
import { typeLogs } from '../../datatemps/TypeLogs';
import moment from 'moment-es6';
import {AlertService} from 'ngx-alerts';

@Component({
  selector: 'app-confirm-approvel',
  templateUrl: './confirm-approvel.component.html',
  styleUrls: ['./confirm-approvel.component.scss']
})
export class ConfirmApprovelComponent implements OnInit {

  public logLeaveForm: FormGroup;
  public title: string;
  public submitText: string;
  public listType: TypeLogLeave[] = typeLogs;


  public submitIsDisabled = false;
  public errorMessageAPI: string;
  public alive = true;

  constructor(
    private fb: FormBuilder,
    private service: LogLeaveService,
    private dialog: MatDialog,
    private alert: AlertService,
    @Inject(MAT_DIALOG_DATA) public dialogData
  ) {
    this.submitIsDisabled = false;
    this.errorMessageAPI = '';
  }


  ngOnInit() {
    this.initDialogData();
    this.initForm();
    this.initFormData();
    this.checkFormValid();
  }
  ngOnDestroy() {
    this.alive = false;
  }


  private initDialogData(): void {

    if(this.dialogData.isApprovel ===1){
      this.submitText = 'APPROVEL';
    }else if(this.dialogData.isApprovel ===2){
      this.submitText = 'REJECT';
    }else if (this.dialogData.isApprovel ===3){
      this.submitText = 'View';
    }else {
      this.submitText = 'REVERT';
    }

    this.title = this.submitText + 'LOG LEAVE';
  }

  private initForm(): void {
    this.logLeaveForm = this.fb.group(
      {
        reason: [{ value: null, disabled: true }],
        comment: [{ value: null, disabled: true }],
        type: [{ value: null, disabled: true }],
        fromDate: [{ value: null, disabled: true }],
        toDate: [{ value: null, disabled: true }],
        fromTime : [{ value: null, disabled: true }],
        toTime : [{ value: null, disabled: true }],
        approvelComment: [{ value: null, disabled: this.dialogData.isApprovel ===3 },Validators.required]
      }
    );

      const fromTime = moment(new Date(this.dialogData.logLeaveDetail.fromDate)).format('HH:mm a');
      const toTime = moment(new Date(this.dialogData.logLeaveDetail.toDate)).format('HH:mm a');

      this.logLeaveForm.patchValue({
        reason: this.dialogData.logLeaveDetail.reason,
        comment: this.dialogData.logLeaveDetail.comment,
        type: this.dialogData.logLeaveDetail.typeID,
        fromDate : new Date(this.dialogData.logLeaveDetail.fromDate),
        toDate : new Date(this.dialogData.logLeaveDetail.toDate),
        fromTime,
        toTime,
        approvelComment: this.dialogData.logLeaveDetail.approvelComment
      });
  }

  public onSubmit(): void {
    this.submitIsDisabled = true;
    if (this.logLeaveForm.valid) {
        const data = this.getFormData(this.dialogData.isApprovel);
        this.service
          .Approvel(data)
          .pipe(takeWhile(() => this.alive))
          .subscribe(
            result => {
              if (result && result.success) {
                this.dialog.closeAll();
                if(this.dialogData.isApprovel ===1){
                  this.alert.success('Approved successfully.');
                }else if(this.dialogData.isApprovel ===2){
                  this.alert.success('Rejected successfully.');
                }else{
                  this.alert.success('Reverted successfully.');
                }
              }
            },
            error => {
              this.errorMessageAPI = error.error.message;
            }
          );
    }
  }

  private getFormData(status: number): any {
    return {
        id : this.dialogData.logLeaveDetail ?  this.dialogData.logLeaveDetail.id as string : '',
        approvelComment: this.logLeaveForm.controls.approvelComment.value,
        status
      };
  }

  private checkFormValid() {
    this.logLeaveForm.valueChanges.subscribe(value => {
      if (value && this.logLeaveForm.valid) {
        this.submitIsDisabled = false;
      } else {
        this.submitIsDisabled = true;
      }
    });
  }

  private initFormData(): void {
    if (this.dialogData.isApprovel) {
      this.logLeaveForm.controls.type.setValue(
        this.listType.find(
          x => x.id === this.dialogData.logLeaveDetail.typeID
        ),
        { onlySelf: true }
      );
    }
  }

  public confirmCancel() {
    this.dialog.closeAll();
  }

}
