import { Component, OnInit,  ViewChild  } from '@angular/core';
import { LogLeaveService } from '../services/logleave.service';
import { GridComponent } from '@progress/kendo-angular-grid';
import { groupBy, GroupDescriptor, State } from '@progress/kendo-data-query';
import { FormGroup } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmApprovelComponent } from './confirm-approvel/confirm-approvel.component';
import {AlertService} from 'ngx-alerts';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-approvelog',
  templateUrl: './approvelog.component.html',
  styleUrls: ['./approvelog.component.scss']
})
export class ApprovelogComponent implements OnInit {

  @ViewChild(GridComponent, { static: false }) grid: GridComponent;
    public groups: GroupDescriptor[] = [];
    public view: any[];
    public formGroup: FormGroup;
    public alive = true;
    public gridState: State = {
      sort: [],
      skip: 0,
      take: 10
  };

    constructor(private service: LogLeaveService,
                private dialog: MatDialog,
                private alert: AlertService,
                private spinner : NgxSpinnerService) {
    }

    public ngOnInit(): void {
      this.searchList();
    }

    public onStateChange(state: State) {
      this.gridState = state;
    }

    public searchList(): void {
        this.spinner.show();
        this.service
        .getListAll()
        .pipe(takeWhile(() => this.alive))
        .subscribe(result => {
            this.view = result.content;
            this.spinner.hide();
        });
    }

    public approveHandler(dataItem : any) {
      const dialogRef = this.dialog.open(ConfirmApprovelComponent, {
            disableClose: true,
            minWidth: '800px',
            panelClass: 'ml-dialog',
            data: {
              isApprovel: 1,
              logLeaveDetail: dataItem,
              callback: () => {
                this.dialog.closeAll();
              }
            }
          });

      dialogRef.afterClosed().subscribe(confirm => {
            this.searchList();
          });
    }

    public rejectHandler(dataItem : any) {
      const dialogRef = this.dialog.open(ConfirmApprovelComponent, {
            disableClose: true,
            minWidth: '800px',
            panelClass: 'ml-dialog',
            data: {
              isApprovel: 2,
              logLeaveDetail: dataItem,
              callback: () => {
                this.dialog.closeAll();
              }
            }
          });

      dialogRef.afterClosed().subscribe(confirm => {
            this.searchList();
          });
    }

    public revertHandler(dataItem : any) {
      const dialogRef = this.dialog.open(ConfirmApprovelComponent, {
            disableClose: true,
            minWidth: '800px',
            panelClass: 'ml-dialog',
            data: {
              isApprovel: 0,
              logLeaveDetail: dataItem,
              callback: () => {
                this.dialog.closeAll();
              }
            }
          });

        dialogRef.afterClosed().subscribe(confirm => {
            this.searchList();
          });
    }

    public viewHandler(dataItem : any) {
      const dialogRef = this.dialog.open(ConfirmApprovelComponent, {
            disableClose: true,
            minWidth: '800px',
            panelClass: 'ml-dialog',
            data: {
              isApprovel: 3,
              logLeaveDetail: dataItem,
              callback: () => {
                this.dialog.closeAll();
              }
            }
          });

        dialogRef.afterClosed().subscribe(confirm => {
            this.searchList();
          });
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.searchList();
        this.view = groupBy(this.view, this.groups);
    }

}
