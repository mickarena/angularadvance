import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogleaveComponent } from './logleave.component';

describe('LogleaveComponent', () => {
  let component: LogleaveComponent;
  let fixture: ComponentFixture<LogleaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogleaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogleaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
