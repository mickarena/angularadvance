import { Component, OnInit, ViewChild  } from '@angular/core';
import { LogLeaveService } from '../services/logleave.service';
import { GridComponent } from '@progress/kendo-angular-grid';
import { groupBy, GroupDescriptor, State } from '@progress/kendo-data-query';
import { FormGroup } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AddupdatelogComponent } from './addupdatelog/addupdatelog.component';
import { ConfirmDialogComponent } from 'src/app/shared/dialog/confirm-dialog/confirm-dialog.component';
import {AlertService} from 'ngx-alerts';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-logleave',
  templateUrl: './logleave.component.html',
  styleUrls: ['./logleave.component.scss']
})
export class LogleaveComponent implements OnInit {

    @ViewChild(GridComponent, { static: false }) grid: GridComponent;
    public groups: GroupDescriptor[] = [];
    public view: any[];
    public formGroup: FormGroup;
    public alive = true;
    public gridState: State = {
      sort: [],
      skip: 0,
      take: 10
  };

    constructor(private service: LogLeaveService,
                private dialog: MatDialog,
                private alert: AlertService,
                private spinner : NgxSpinnerService) {
    }

    public ngOnInit(): void {
      this.searchList();
    }

    public onStateChange(state: State) {
      this.gridState = state;
    }

    public searchList(): void {
        this.spinner.show();
        this.service
        .getList()
        .pipe(takeWhile(() => this.alive))
        .subscribe(result => {
            this.view = result.content;
            this.spinner.hide();
        });
    }

    public addHandler({sender}) {
      const dialogRef = this.dialog.open(AddupdatelogComponent, {
            disableClose: true,
            minWidth: '800px',
            panelClass: 'ml-dialog',
            data: {
              isEdit: false
            }
          });

      dialogRef.afterClosed().subscribe(confirm => {
        debugger;
             //this.alert.success('Added successfully.');
             this.searchList();
          });

    }

    public removeHandler({dataItem}) {
      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: 'Are you sure you want to delete item?',
          confirmButton: 'Yes',
          cancelButton: 'No'
        }
      });

      dialogRef.afterClosed().subscribe(confirm => {
        if (confirm) {
          this.service.delete(dataItem.id).subscribe((res) => {
            if (res.success) {
              this.alert.success('Deleted successfully.');
            } else {
              this.alert.warning(res.message);
            }
            this.searchList();
          }, err => {
              this.alert.warning('Log Leave has been deleted failed');
          });
        }
      });

    }

    public editHandler({sender, rowIndex, dataItem}) {
      const dialogRef = this.dialog.open(AddupdatelogComponent, {
            disableClose: true,
            minWidth: '800px',
            panelClass: 'ml-dialog',
            data: {
              isEdit: true,
              logLeaveDetail: dataItem,
              callback: () => {
                this.dialog.closeAll();
              }
            }
          });

      dialogRef.afterClosed().subscribe(confirm => {
            //this.alert.success('Edited successfully.');
            this.searchList();
          });
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.searchList();
        this.view = groupBy(this.view, this.groups);
    }
}
