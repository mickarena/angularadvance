import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { takeWhile } from 'rxjs/operators';
import { LogLeave } from '../../models/log-leave.model';
import { LogLeaveService } from '../../services/logleave.service';
import { CustomValidator } from 'src/app/shared/validators/custom-validator';
import { TypeLogLeave } from '../../models/type-log-leave.mode';
import { typeLogs } from '../../datatemps/TypeLogs';
import moment from 'moment-es6';
import {AlertService} from 'ngx-alerts';

@Component({
  selector: 'app-addupdatelog',
  templateUrl: './addupdatelog.component.html',
  styleUrls: ['./addupdatelog.component.scss']
})
export class AddupdatelogComponent  implements OnInit, OnDestroy {
  public logLeaveForm: FormGroup;
  public title: string;
  public submitText: string;
  public listType: TypeLogLeave[] = typeLogs;


  public submitIsDisabled = false;
  public errorMessageAPI: string;
  public alive = true;

  constructor(
    private fb: FormBuilder,
    private service: LogLeaveService,
    private dialog: MatDialog,
    private alert: AlertService,
    @Inject(MAT_DIALOG_DATA) public dialogData
  ) {
    this.submitIsDisabled = false;
    this.errorMessageAPI = '';
  }


  ngOnInit() {
    this.initDialogData();
    this.initForm();
    this.initFormData();
    this.checkFormValid();
  }
  ngOnDestroy() {
    this.alive = false;
  }


  private initDialogData(): void {
    this.title = this.dialogData.isEdit
      ? 'EDIT LOG LEAVE'
      : 'CREATE NEW LOG LEAVE';
    this.submitText = this.dialogData.isEdit ? 'UPDATE' : 'CREATE';
  }

  private initForm(): void {
    this.logLeaveForm = this.fb.group(
      {
        reason: [null,
          [
            Validators.required,
            CustomValidator.noWhitespaceValidator,
            Validators.maxLength(100)
          ]
        ],
        comment: [null,
          [
            Validators.required,
            CustomValidator.noWhitespaceValidator,
            Validators.maxLength(100)
          ]
        ],
        type: [null, Validators.required],
        fromDate: [null, Validators.required],
        toDate: [null, Validators.required],
        fromTime : [null, Validators.required],
        toTime : [null, Validators.required]
      },{validators:  [this.checkCompareFromDate_ToDate]}
    );
    if (this.dialogData.isEdit) {
      const fromTime = moment(new Date(this.dialogData.logLeaveDetail.fromDate)).format('HH:mm a');
      const toTime = moment(new Date(this.dialogData.logLeaveDetail.toDate)).format('HH:mm a');

      this.logLeaveForm.patchValue({
        reason: this.dialogData.logLeaveDetail.reason,
        comment: this.dialogData.logLeaveDetail.comment,
        type: this.dialogData.logLeaveDetail.typeID,
        fromDate : new Date(this.dialogData.logLeaveDetail.fromDate),
        toDate : new Date(this.dialogData.logLeaveDetail.toDate),
        fromTime,
        toTime
      });
    }
  }

  public onSubmit(): void {
    this.submitIsDisabled = true;
    if (this.logLeaveForm.valid) {
      if (!this.dialogData.isEdit) {
        this.service
          .create(this.getFormData())
          .pipe(takeWhile(() => this.alive))
          .subscribe(
            result => {
              if (result && result.success) {
                this.dialog.closeAll();
                this.alert.success('Added successfully.');
              }
            },
            error => {
              this.errorMessageAPI = error.error.message;
            }
          );
      } else {
        this.service
          .update(this.getFormData())
          .pipe(takeWhile(() => this.alive))
          .subscribe(
            result => {
              if (result && result.success) {
                this.dialogData.callback();
                this.alert.success('Edited successfully.');
              }
            },
            error => {
              this.errorMessageAPI = error.error.message;
            }
          );
      }
    }
  }

  private getFormData(): LogLeave {

   const fromTime = this.logLeaveForm.controls.fromTime.value;
   const fromDate = this.formatDateValue(
    this.logLeaveForm.controls.fromDate.value
   );
   const fromDateString = fromDate.split(',')[0] + ', ' + fromTime;


   const toTime = this.logLeaveForm.controls.toTime.value;
   const toDate = this.formatDateValue(
    this.logLeaveForm.controls.toDate.value
   );
   const toDateString = toDate.split(',')[0] + ', ' + toTime;


    return {
        typeId: this.logLeaveForm.controls.type.value.id,
        fromDate :   fromDateString,
        toDate: toDateString,
        reason: this.logLeaveForm.controls.reason.value,
        comment: this.logLeaveForm.controls.comment.value,
        id : this.dialogData.logLeaveDetail ?  this.dialogData.logLeaveDetail.id as string : '',
        approver : '',
        status : '',
        userId : '',
        approverName : '',
        createDate : '',
        statusName : '',
        typeName : '',
        userFullName : ''
      };
  }

  private checkFormValid() {
    this.logLeaveForm.valueChanges.subscribe(value => {
      if (value && this.logLeaveForm.valid) {
        this.submitIsDisabled = false;
      } else {
        this.submitIsDisabled = true;
      }
    });
  }

  private initFormData(): void {
    if (this.dialogData.isEdit) {
      this.logLeaveForm.controls.type.setValue(
        this.listType.find(
          x => x.id === this.dialogData.logLeaveDetail.typeID
        ),
        { onlySelf: true }
      );
    }
  }

  public confirmCancel() {
    this.dialog.closeAll();
  }

  checkCompareFromDate_ToDate (group: FormGroup) {
    if(!group.controls.fromDate.value || !group.controls.toDate.value) {
      return true;
    }
    const fromTime = group.controls.fromTime.value;
    const fromDate =  moment( group.controls.fromDate.value).format('YYYY-MM-DD');
    const fromDateString =fromTime? fromDate.split(',')[0] + ', ' + fromTime : fromDate.split(',')[0];

    const toTime = group.controls.toTime.value;
    const toDate = moment(group.controls.toDate.value).format('YYYY-MM-DD');
    const toDateString =toTime?  toDate.split(',')[0] + ', ' + toTime : toDate.split(',')[0];

    return  (new Date(toDateString)) >= (new Date(fromDateString)) ? null : { toDateMoreThanFromDate: true }
  }

  private formatDateValue(date: Date): string {
    const dateString = moment(date).format('YYYY-MM-DD');
    return dateString;
  }

  disableWeekend(date: Date) {
    const d = new Date(date);
    if (d.getDay() !== 0 && d.getDay() !== 6  ) {
      return d;
    }
  }

}
