import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddupdatelogComponent } from './addupdatelog.component';

describe('AddupdatelogComponent', () => {
  let component: AddupdatelogComponent;
  let fixture: ComponentFixture<AddupdatelogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddupdatelogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddupdatelogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
