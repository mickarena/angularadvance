export interface DialogModel {
  title: string;
  confirmButton: string;
  cancelButton: string;
  color?: string;
  isHtmlBody?: boolean;
  isHtmlTitle?: boolean;
  body?: string;

}
