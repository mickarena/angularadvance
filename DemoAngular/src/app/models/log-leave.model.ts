export interface LogLeave {
    id: string;
    userId: string;
    approver: string;
    typeId: string;
    fromDate: string;
    toDate: string;
    reason: string;
    comment: string;
    status: string;
    typeName: string;
    userFullName: string;
    approverName: string;
    createDate: string;
    statusName: string;
  }

