export interface ResponseModel {
    content: any;
    code: number;
    success: boolean;
    message?: string;
    totalRecords?: number;
    currentPage?: number;
  }