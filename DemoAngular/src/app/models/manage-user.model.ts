export interface ManageUser {
    id: string;
    email: string;
    code: string;
    // Password: string;
    // ConfirmPassword: string;
    userName: string;
    fullName: string;
    birthday: string;
}
