import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConfigService } from './shared/config.service';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
/* Module Imports */
import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
import { AccountModule } from './account/account.module';
import { ShellModule } from './shell/shell.module';
import { TopSecretModule } from './top-secret/top-secret.module';
import { SharedModule } from './shared/shared.module';
import { ConfirmDialogComponent } from 'src/app/shared/dialog/confirm-dialog/confirm-dialog.component';
import { AlertModule } from 'ngx-alerts';
import { NgxSpinnerModule  } from 'ngx-spinner';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import { GridModule } from '@progress/kendo-angular-grid';
import { ManageuserComponent } from './manageuser/manageuser.component';
import { LogleaveComponent } from './logleave/logleave.component';
import { ApprovelogComponent } from './approvelog/approvelog.component';
import { ConfirmApprovelComponent } from './approvelog/confirm-approvel/confirm-approvel.component';

import { ManageUserService } from './services/manageuser.service';
import { LogLeaveService } from './services/logleave.service';
import { AddupdatelogComponent } from './logleave/addupdatelog/addupdatelog.component';
import { MaterialModule } from './shared/material.module';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';


export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/DD/YYYY',
  },
  display: {
    dateInput: 'MM/DD/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  declarations: [
      AppComponent,
      AuthCallbackComponent,
      ManageuserComponent,
      LogleaveComponent,
      ApprovelogComponent,
      AddupdatelogComponent,
      ConfirmDialogComponent,
      ConfirmApprovelComponent
   ],
  imports: [
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    // CoreModule.forRoot(),
    CoreModule,
    HomeModule,
    AccountModule,
    TopSecretModule,
    AppRoutingModule,
    ShellModule,
    SharedModule,
    GridModule,
    MaterialModule,
    AlertModule.forRoot({maxMessages: 5, timeout: 5000, positionX: 'right', positionY: 'top'}),
    NgxSpinnerModule,

    MatDatepickerModule,
    NgxMaterialTimepickerModule,
    PopupModule,
    DateInputsModule
  ],
  providers: [ConfigService,
      ManageUserService,
      LogLeaveService
    ],
  bootstrap: [AppComponent]
  ,
  entryComponents: [AddupdatelogComponent , ConfirmDialogComponent]
})
export class AppModule { }
