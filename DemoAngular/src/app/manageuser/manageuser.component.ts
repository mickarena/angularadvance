import { Component, OnInit } from '@angular/core';
import { ManageUserService } from '../services/manageuser.service';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State , process } from '@progress/kendo-data-query';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { ManageUser } from '../models/manage-user.model';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/shared/dialog/confirm-dialog/confirm-dialog.component';
import {AlertService} from 'ngx-alerts';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-manageuser',
  templateUrl: './manageuser.component.html',
  styleUrls: ['./manageuser.component.scss']
})
export class ManageuserComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public alive = true;
    public formGroup: FormGroup;
    public maxDate: Date = new Date();


    private editedRowIndex: number;

    constructor(private service: ManageUserService,
                private dialog: MatDialog,
                private alert: AlertService,
                private spinner : NgxSpinnerService) {
                  this.maxDate.setHours(23, 59, 59);
                }


    public ngOnInit(): void {
        this.search();
    }

    public search(): void {
        this.spinner.show();
        this.service
        .getList()
        .pipe(takeWhile(() => this.alive))
        .subscribe(result => {
            this.view = result.content;
            process(result.content, this.gridState);
            this.spinner.hide();
        });
    }

    public onStateChange(state: State) {
        this.gridState = state;
    }

    public addHandler({sender}) {
        this.closeEditor(sender);
        this.formGroup = new FormGroup({
            id: new FormControl(),
            userName: new FormControl('', [Validators.required, Validators.email]),
            code: new FormControl('', Validators.compose([Validators.required, Validators.minLength(6)])),
            fullName: new FormControl('', [Validators.required, Validators.minLength(6)]),
            birthday: new FormControl(null),
            phoneNumber : new FormControl('',  [Validators.required, Validators.pattern('^[0-9]{10,11}')]),
        });

        sender.addRow(this.formGroup);
    }

    public editHandler({sender, rowIndex, dataItem}) {
        this.closeEditor(sender);
        this.formGroup = new FormGroup({
            id: new FormControl(dataItem.id),
            userName: new FormControl(dataItem.userName, [Validators.required, Validators.email]),
            code: new FormControl( dataItem.code, Validators.compose([Validators.required, Validators.minLength(6)])),
            fullName: new FormControl(dataItem.fullName, [Validators.required, Validators.minLength(6)]),
            birthday: new FormControl( dataItem.birthday ? new Date(dataItem.birthday) : null),
            phoneNumber : new FormControl( dataItem.phoneNumber , [Validators.required, Validators.pattern('^[0-9]{10,11}')]),
        });

        this.editedRowIndex = rowIndex;

        sender.editRow(rowIndex, this.formGroup);
    }

    public cancelHandler({sender, rowIndex}) {
        this.closeEditor(sender, rowIndex);
    }

    public saveHandler({sender, rowIndex, formGroup, isNew}) {
      if (isNew) {
         const user: ManageUser = formGroup.value;
         this.service.create(user).subscribe(response => {
            if (response && response.success) {
                this.search();
                this.cancelHandler({sender, rowIndex});
                this.alert.success('Created successfully.');
            } else {
              this.alert.warning(response.message);
            }
            }, () => {
              this.alert.warning('Save failed. Please try again.');
            });
      } else {
        const user: ManageUser = formGroup.value;
        this.service.update(user).subscribe(response => {
           if (response && response.success) {
               this.search();
               this.cancelHandler({sender, rowIndex});
               this.alert.success('Updated successfully.');
           } else {
             this.alert.warning(response.message);
           }
           }, () => {
             this.alert.warning('Save failed. Please try again.');
           });
      }
    }

    public removeHandler({dataItem}) {

        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
              title: 'Are you sure you want to delete item?',
              confirmButton: 'Yes',
              cancelButton: 'No'
            }
          });

        dialogRef.afterClosed().subscribe(confirm => {
            if (confirm) {
                this.service.delete(dataItem.id).subscribe((res) => {
                    if (res.success) {
                      this.alert.success('Deleted successfully.');
                    } else {
                      this.alert.warning(res.message);
                    }
                    this.search();
                  }, err => {
                    this.alert.warning('User has been deleted failed');
                  });
            }
          });
    }

    private closeEditor(grid, rowIndex = this.editedRowIndex) {
        grid.closeRow(rowIndex);
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

}
