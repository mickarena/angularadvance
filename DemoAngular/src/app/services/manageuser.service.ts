import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResponseModel } from '../models/responses.model';
import { AuthService } from '../core/authentication/auth.service';
import { environment } from '../../environments/environment';
import { ManageUser } from '../models/manage-user.model';

import {
    MANAGE_USER_API_GETUSERADMIN,
    MANAGE_USER_API_GET,
    MANAGE_USER_API_CREATE,
    MANAGE_USER_API_UPDATE,
    MANAGE_USER_API_DELETE
  } from '../shared/constants/api.constants';

@Injectable()
export class ManageUserService {

    constructor(private http: HttpClient , private authService: AuthService)
    { }

    public getListUserAdmin(): Observable<ResponseModel> {
        return this.http.get<ResponseModel>(environment.apiBaseUrl + MANAGE_USER_API_GETUSERADMIN , this.authService.getRequestHeaders());
    }

    public getList(): Observable<ResponseModel> {
        return this.http.get<ResponseModel>(environment.apiBaseUrl + MANAGE_USER_API_GET, this.authService.getRequestHeaders());
    }

    public create(data: ManageUser): Observable<ResponseModel> {
      return this.http.post<ResponseModel>(environment.apiBaseUrl + MANAGE_USER_API_CREATE, data, this.authService.getRequestHeaders());
    }

    public update(data: ManageUser): Observable<ResponseModel> {
      return this.http.put<ResponseModel>(environment.apiBaseUrl + MANAGE_USER_API_UPDATE, data, this.authService.getRequestHeaders());
    }

    public delete(id: string): Observable<ResponseModel> {
      return this.http.get<ResponseModel>(environment.apiBaseUrl + `${MANAGE_USER_API_DELETE}?id=${id}`,
       this.authService.getRequestHeaders());
    }
}
