import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseModel } from '../models/responses.model';
import { AuthService } from '../core/authentication/auth.service';
import { environment } from '../../environments/environment';
import { LogLeave } from '../models/log-leave.model';

import {
    LOG_LEAVE_API_GET,
    LOG_LEAVE_API_GETAll,
    LOG_LEAVE_API_CREATE,
    LOG_LEAVE_API_UPDATE,
    LOG_LEAVE_API_DELETE,
    LOG_LEAVE_API_APPROVEL,
    LOG_LEAVE_API_REJECT,
  } from '../shared/constants/api.constants';


@Injectable({
  providedIn: 'root'
})
export class LogLeaveService {

  constructor(private http: HttpClient , private authService : AuthService) { }

  public getList(): Observable<ResponseModel> {
      return this.http.get<ResponseModel>(environment.apiBaseUrl + LOG_LEAVE_API_GET, this.authService.getRequestHeaders());
  }

  public getListAll(): Observable<ResponseModel> {
    return this.http.get<ResponseModel>(environment.apiBaseUrl + LOG_LEAVE_API_GETAll, this.authService.getRequestHeaders());
}

  public create(data: LogLeave): Observable<ResponseModel> {
    return this.http.post<ResponseModel>(environment.apiBaseUrl +LOG_LEAVE_API_CREATE, data , this.authService.getRequestHeaders());
  }

  public update(data: LogLeave): Observable<ResponseModel> {
    return this.http.put<ResponseModel>(environment.apiBaseUrl + LOG_LEAVE_API_UPDATE, data , this.authService.getRequestHeaders());
  }

  public delete(id: string): Observable<ResponseModel> {
    return this.http.get<ResponseModel>(environment.apiBaseUrl + `${LOG_LEAVE_API_DELETE}?id=${id}`, this.authService.getRequestHeaders());
  }

  public Approvel(data : any): Observable<ResponseModel> {
    return this.http.post<ResponseModel>(environment.apiBaseUrl +LOG_LEAVE_API_APPROVEL, data , this.authService.getRequestHeaders());
  }
}
