import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { ManageuserComponent } from './manageuser/manageuser.component';
import { LogleaveComponent } from './logleave/logleave.component';
import { ApprovelogComponent } from './approvelog/approvelog.component';
import { Shell } from './shell/shell.service';
import {  RoleGuardService as RoleGuard } from './core/authentication/role-guard.service';

const routes: Routes = [
  { path: 'auth-callback', component: AuthCallbackComponent  },
  Shell.childRoutes([
    { path: 'manageuser',
     component: ManageuserComponent,
     canActivate: [RoleGuard],
      data: {
        expectedRole: 'admin'
      }
    },
    { path: 'approvelleave',
      component: ApprovelogComponent,
      canActivate: [RoleGuard],
      data: {
        expectedRole: 'admin'
      }
    },
    { path: 'logleave',
      component: LogleaveComponent,
      canActivate: [RoleGuard],
      data: {
        expectedRole: 'employee'
      }
    }
  ])
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
