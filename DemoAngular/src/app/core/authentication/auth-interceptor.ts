import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, Observer, throwError } from 'rxjs';

import { catchError } from 'rxjs/operators';

import { AppEventRegistry } from '../../../environments/app-event-registry';
import { AuthService } from './auth.service';

interface QueueItem {
  request: HttpRequest<any>;
  handler: HttpHandler;
  observer: Observer<HttpEvent<any>>;
}

let queue: QueueItem[];

function init(registry: AppEventRegistry) {
  if (queue) {
    return;
  }

  queue = [];

  registry.subscribe<string>(AppEventRegistry.TOKEN_REFRESHED,
    (newToken: string) => retryQueue(newToken));
}

function retryQueue(newToken: string) {
  const authHeader = 'Bearer ' + newToken;
  let item: QueueItem;
  while (queue.length > 0) {
    item = queue.pop();
    const request = item.request.clone({setHeaders: {Authorization: authHeader} });

    const observer = item.observer;
    item.handler.handle(request)
      .subscribe((response) => {
        observer.next(response);
        observer.complete();
      }, (error) => observer.error(error));
  }
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  
  constructor(private eventRegistry: AppEventRegistry,
              private authenticationService: AuthService) {
    init(eventRegistry);
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let request = req;
    const token = this.authenticationService.getAccessToken();
    if (token) {
      const authHeader = 'Bearer ' + token;
      request = req.clone({setHeaders: {Authorization: authHeader}});
    }

    return next.handle(request)
      .pipe(catchError((err: HttpErrorResponse, caught: Observable<HttpEvent<any>>) => {
        if (err && err.status === 401) {
          return new Observable<HttpEvent<any>>((observer: Observer<HttpEvent<any>>) => {
            queue.push({
              request,
              handler: next,
              observer
            });

            if (queue.length > 0) {
              // limit this trigger to avoid leakage
              this.eventRegistry.trigger(AppEventRegistry.UNAUTHORIZED, null);
            }

            throw err;
          });
        }

        return throwError(err);
      }));
  }
}
