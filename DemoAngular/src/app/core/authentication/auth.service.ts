import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { UserManager, UserManagerSettings, User } from 'oidc-client';
import { BehaviorSubject, from } from 'rxjs';
import { BaseService } from '../../shared/base.service';
import { ConfigService } from '../../shared/config.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService  {

  // Observable navItem source
  private _authNavStatusSource = new BehaviorSubject<boolean>(false);
  // Observable navItem stream
  authNavStatus$ = this._authNavStatusSource.asObservable();

  private manager = new UserManager(getClientSettings());
  private user: User | null;

  constructor(private http: HttpClient, private configService: ConfigService) {
    super();

    this.manager.getUser().then(user => {
      this.user = user;
      this._authNavStatusSource.next(this.isAuthenticated());
    });
  }

  login() {
    return this.manager.signinRedirect();
  }

  async completeAuthentication() {
      this.user = await this.manager.signinRedirectCallback();
      this._authNavStatusSource.next(this.isAuthenticated());
  }

  public getRequestHeaders(): { headers: HttpHeaders | { [header: string]: string | string[]; } } {
    const headers = new HttpHeaders({
        Authorization: 'Bearer ' + this.user.access_token,
        'Content-Type': 'application/json',
        // 'Accept': `application/vnd.iman.v${EndpointFactory.apiVersion}+json, application/json, text/plain, */*`,
        // 'App-Version': ConfigurationService.appVersion
    });
    return { headers };
  }

  register(userRegistration: any) {
    return this.http.post(this.configService.authApiURI + '/account', userRegistration).pipe(catchError(this.handleError));
  }

  isAuthenticated(): boolean {
    return this.user != null && !this.user.expired;
  }

  public getAccessToken(): string {
    return this.user.access_token;
  }

  get authorizationHeaderValue(): string {
    return `${this.user.token_type} ${this.user.access_token}`;
  }

  get name(): string {
    return this.user != null ? this.user.profile.name : '';
  }

  get roles(): string {
    return this.user != null  ? this.user.profile.roles : '';
  }

  async signout() {
    await this.manager.signoutRedirect();
  }
}

export function getClientSettings(): UserManagerSettings {
  // return {
  //     authority: environment.apiBaseUrl,
  //     // authority: 'https://localhost:44322',
  //     client_id: 'spa',
  //     redirect_uri: 'https://localhost:5100/auth-callback',
  //     post_logout_redirect_uri: 'https://localhost:5100',
  //     response_type: 'code',
  //     // scope:"openid profile email api.read",
  //     scope: 'email openid profile roles',
  //     filterProtocolClaims: true,
  //     loadUserInfo: true,
  //     automaticSilentRenew: true,
  //     silent_redirect_uri: 'https://localhost:5100/silent-refresh.html'
  // };


    return {
      authority: environment.apiBaseUrl,
      client_id: 'angular_spa',
      redirect_uri: 'http://localhost:4200/auth-callback',
      post_logout_redirect_uri: 'http://localhost:4200',
      response_type: 'code' ,
      scope:'email openid profile roles',
      filterProtocolClaims: true,
      loadUserInfo: true,
      automaticSilentRenew: true,
      silent_redirect_uri: 'http://localhost:4200/silent-refresh.html'
  };


}



// "options": {
//   "browserTarget": "DemoProject:build",
//   "port": 5100,
//   "ssl": true,
//   "sslCert": "localhost.crt",
//   "sslKey": "localhost.key"
// },

