import { NgModule, Optional, SkipSelf , ModuleWithProviders } from '@angular/core'; 
import { AuthService } from './authentication/auth.service';
import { AuthGuard } from './authentication/auth.guard';
import { RoleGuardService } from './authentication/role-guard.service';

import { EnvironmentModule } from 'src/environments/environment.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './authentication/auth-interceptor';

@NgModule({
  imports: [
  ],
  providers: [
    AuthService,
    AuthGuard,
    RoleGuardService
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    // Import guard
    if (parentModule) {
      throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
    }
  }
}