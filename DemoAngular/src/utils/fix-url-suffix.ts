export function fixUrlSuffix(url: string): string {
    if (url && url[url.length - 1] !== '/') {
      return url + '/';
    }
  
    return url;
  }
  