export function forceGetValueFromNullObject(object: any, property: string) {
  return object !== null ? object[property] : null;
}

export function dynamicSort(property, order) {
  let sortOrder = 1;
  if (order === 'desc') {
    sortOrder = -1;
  }
  // tslint:disable-next-line:only-arrow-functions
  return function(a, b) {
    if (a[property] < b[property]) {
      return -1 * sortOrder;
    } else if (a[property] > b[property]) {
      return 1 * sortOrder;
    } else {
      return 0 * sortOrder;
    }
  };
}

export function removeSpecificError(formControl, error: string) {
  const err = formControl.errors;
  if (err && Object.keys(err).length === 1 && err[error]) {
    formControl.setErrors(null);
  }
}
