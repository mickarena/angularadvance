
import {HttpClient , HttpHeaders,HttpParams} from '@angular/common/http';

export const environment = {
  production: false,
  //apiBaseUrl:"https://localhost:44322",
  apiBaseUrl:"http://hoangvh030490-001-site1.itempurl.com",
  headers :  new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }),
  httpHeaders :  new HttpHeaders({'Content-Type':  'application/x-www-form-urlencoded' }),
  tokenUrl: null, // For IdentityServer/Authorization Server API. You can set to null if same as baseUrl
  loginUrl: '/login'
};
