import { Injectable } from '@angular/core';
import { fixUrlSuffix } from '../utils/fix-url-suffix';

export interface Config {
  identityBaseUrl: string;
  apiBaseUrl: string;
  postLogoutRedirectUri: string;
}

let identityBaseUrl = '';
let apiBaseUrl = '';
let postLogoutRedirectUri = '';

if (typeof(localStorage) !== 'undefined') {
  identityBaseUrl = fixUrlSuffix("https://localhost:44322");
  apiBaseUrl = fixUrlSuffix("http://hoangvh030490-001-site1.itempurl.com");
  //postLogoutRedirectUri = fixUrlSuffix(localStorage.getItem('postLogoutRedirectUri'));
}

@Injectable()
export class ConfigService {
  public get(): Config {
    return {
      identityBaseUrl,
      apiBaseUrl,
      postLogoutRedirectUri
    };
  }
}
