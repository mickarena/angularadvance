import { Injectable, EventEmitter } from '@angular/core';

const events: { [id: string]: EventEmitter<any> } = {};

@Injectable()
export class AppEventRegistry {
  public static TOKEN_REFRESHED = 'TokenRefreshed';
  public static UNAUTHORIZED = 'Unauthorized';
  public static LANGUAGE_CHANGED = 'LanguageChanged';
  public static SESSION_DATA_CHANGED = 'SessionDataChanged';
  public static USER_CHANGED = 'UserChanged';

  public static TOKEN_RENEW_FAILED = 'TokenRenewFailed';

  public subscribe<T>(eventId: string, generator: (param: T) => any): any {
      const event = this.getEvent<T>(eventId);
      return event.subscribe(generator);
  }

  public trigger<T>(eventId: string, param: T) {
    const event = this.getEvent<T>(eventId);
    event.emit(param);
  }

  public getEvent<T>(eventId: string): EventEmitter<T> {
      let event: EventEmitter<T> = events[eventId];

      if (!event) {
          event = new EventEmitter<T>();
          events[eventId] = event;
      }

      return event;
  }
}

export const appEventRegistry = new AppEventRegistry();
