import { NgModule } from '@angular/core';
import { ConfigService } from './config.service';
import { AppEventRegistry } from './app-event-registry';

@NgModule({
  imports: [],
  providers: [
    ConfigService,
    AppEventRegistry
  ]
})
export class EnvironmentModule { }
