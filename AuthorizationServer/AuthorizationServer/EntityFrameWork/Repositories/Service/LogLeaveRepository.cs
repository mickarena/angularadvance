﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AuthorizationServer.Models;
using AuthorizationServer.Repositories.Interfaces;

namespace AuthorizationServer.Repositories.Service
{
    public class LogLeaveRepository : Repository<LogLeave>, ILogLeaveRepository
    {
        public LogLeaveRepository(ApplicationDbContext context) : base(context)
        { }
    }
}
