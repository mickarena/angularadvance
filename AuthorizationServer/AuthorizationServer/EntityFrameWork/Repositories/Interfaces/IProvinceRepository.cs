﻿using AuthorizationServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AuthorizationServer.Repositories.Interfaces
{
    public interface IProvinceRepository : IRepository<Province>
    {
    }
}
