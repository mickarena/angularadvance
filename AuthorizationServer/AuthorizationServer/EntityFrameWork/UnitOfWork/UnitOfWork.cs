﻿// =============================
// Email: info@ebenmonney.com
// www.ebenmonney.com/templates
// =============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuthorizationServer.Models;
using AuthorizationServer.Repositories.Interfaces;
using AuthorizationServer.Repositories.Service;

namespace AuthorizationServer.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;

        ILogLeaveRepository _logLeaves;
        IProvinceRepository _provinces;


        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        public ILogLeaveRepository LogLeaves
        {
            get
            {
                if (_logLeaves == null)
                    _logLeaves = new LogLeaveRepository(_context);
                return _logLeaves;
            }
        }

        public IProvinceRepository Provinces
        {
            get
            {
                if (_provinces == null)
                    _provinces = new ProvinceRepository(_context);
                return _provinces;
            }
        }


        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
