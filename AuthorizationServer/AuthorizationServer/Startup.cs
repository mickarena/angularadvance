﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using AuthorizationServer.Models;
using AuthorizationServer.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using OpenIddict.Abstractions;
using OpenIddict.Core;
using OpenIddict.EntityFrameworkCore.Models;
using System.Linq;
using AuthorizationServer.UnitOfWork;
using AuthorizationServer.Repositories.Interfaces;
using AuthorizationServer.Repositories.Service;

namespace AuthorizationServer
{
    public class Startup
    {

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                                  builder =>
                                  {
                                      builder.WithOrigins("http://localhost:4200" , "https://localhost:5100")
                                                  .AllowAnyHeader()
                                                  .AllowAnyMethod(); 
                                  });
            });

            services.AddMvc();

          

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                // Configure the context to use Microsoft SQL Server.
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                options.UseOpenIddict();
            });

            // Register the Identity services.
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
            });

            services.AddOpenIddict()

                // Register the OpenIddict core services.
                .AddCore(options =>
                {
                    // Register the Entity Framework stores and models.
                    options.UseEntityFrameworkCore()
                           .UseDbContext<ApplicationDbContext>();
                })

                // Register the OpenIddict server handler.
                .AddServer(options =>
                {
                    options.UseMvc();

                    // Enable the authorization, logout, token and userinfo endpoints.
                    options
                           .EnableAuthorizationEndpoint("/connect/authorize")
                           .EnableLogoutEndpoint("/connect/logout")
                           .EnableTokenEndpoint("/connect/token")
                           .EnableUserinfoEndpoint("/api/userinfo");

                    // Mark the "email", "profile" and "roles" scopes as supported scopes.
                    options.RegisterScopes(OpenIdConnectConstants.Scopes.Email,
                                           OpenIdConnectConstants.Scopes.Profile,
                                           OpenIddictConstants.Scopes.Roles);

                    options.AllowAuthorizationCodeFlow();

                    options.EnableRequestCaching();

                    // During development, you can disable the HTTPS requirement.
                    options.DisableHttpsRequirement();
                })

                .AddValidation();

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();


            services.AddScoped<IUnitOfWork, HttpUnitOfWork>();
            //services.AddTransient<IDatabaseInitializer, DatabaseInitializer>();

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            //});
        }

        public void Configure(IApplicationBuilder app)
        {


            app.UseDeveloperExceptionPage();

            app.UseStaticFiles();

            app.UseStatusCodePagesWithReExecute("/error");

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthentication();

            app.UseMvcWithDefaultRoute();


            app.Use(async (ctx, next) =>
            {
                await next();
                if (ctx.Response.StatusCode == 204)
                {
                    ctx.Response.ContentLength = 0;
                }
            });

            InitializeAsync(app.ApplicationServices).GetAwaiter().GetResult();


            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("v1/swagger.json", "MyAPI V1");
            //});

        }

        private async Task InitializeAsync(IServiceProvider services)
        {
            // Create a new service scope to ensure the database context is correctly disposed when this methods returns.
            using (var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                await context.Database.EnsureCreatedAsync();

                var manager = scope.ServiceProvider.GetRequiredService<OpenIddictApplicationManager<OpenIddictApplication>>();



                var listClient = Configuration.GetSection("ListClient").Get<List<ClientInfor>>();
                foreach(var item in listClient)
                {
                    var app = await manager.FindByClientIdAsync(item.ClientId);
                    if (app == null)
                    {
                        var descriptor = new OpenIddictApplicationDescriptor
                        {

                            ClientId = item.ClientId,
                            //ClientSecret = item.ClientSecret,
                            DisplayName = item.DisplayName,
                            RedirectUris = { new Uri(item.RedirectUris[0]) },
                            PostLogoutRedirectUris = { new Uri(item.PostLogoutRedirectUris[0])},
                            Permissions =
                                            {
                                                OpenIddictConstants.Permissions.Endpoints.Authorization,
                                                OpenIddictConstants.Permissions.Endpoints.Logout,
                                                OpenIddictConstants.Permissions.Endpoints.Token,
                                                OpenIddictConstants.Permissions.GrantTypes.AuthorizationCode,
                                                OpenIddictConstants.Permissions.GrantTypes.RefreshToken,
                                                OpenIddictConstants.Permissions.Scopes.Email,
                                                OpenIddictConstants.Permissions.Scopes.Profile,
                                                OpenIddictConstants.Permissions.Scopes.Roles
                                            }
                            
              
                        };

                        await manager.CreateAsync(descriptor);
                    }
                   
                }



                if (await manager.FindByClientIdAsync("postman") == null)
                {
                    var descriptor = new OpenIddictApplicationDescriptor
                    {
                        ClientId = "postman",
                        DisplayName = "Postman",
                        RedirectUris = { new Uri("https://www.getpostman.com/oauth2/callback") },
                        Permissions =
                        {
                            OpenIddictConstants.Permissions.Endpoints.Authorization,
                            OpenIddictConstants.Permissions.Endpoints.Token,
                            OpenIddictConstants.Permissions.GrantTypes.AuthorizationCode,
                            OpenIddictConstants.Permissions.Scopes.Email,
                            OpenIddictConstants.Permissions.Scopes.Profile,
                            OpenIddictConstants.Permissions.Scopes.Roles
                        }
                    };

                    await manager.CreateAsync(descriptor);
                }
            }
        }
    }
}
