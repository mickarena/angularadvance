﻿using AuthorizationServer.Models;
using AuthorizationServer.Repositories.Interfaces;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;
using static System.Net.Mime.MediaTypeNames;

namespace AuthorizationServer.Controllers
{
    public class HomeController : Controller
    {

        private readonly UserManager<ApplicationUser> _userManager;


        public HomeController(UserManager<ApplicationUser> userManager )
        {
            _userManager = userManager;
        }

        public IActionResult Index()
        {

            //var user =  new ApplicationUser();
            //if (User.Identity.IsAuthenticated)
            //{
            //    user.UserName = 
            //}
            ////_userManager.GetUserAsync(User)

            //if (user != null)
            //{
            //    //var userDetail = _userManager.GetUserAsync;
            //}


            return View();
        }


        [HttpGet]
        [Route(".well-known/acme-challenge/{id}")]
        public ActionResult LetsEncrypt(string id)
        {

            //var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            //var directoryPath = Path.GetDirectoryName(location);


             var file = Path.Combine(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory), ".well-known", "acme-challenge", id);

            //var file = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), ".well-known", "acme-challenge", id);

            //Server.MapPath("~/.well-known/acme-challenge/" + id);

            //var file = Path.Combine("~/.well-known/acme-challenge/"+id);
            return PhysicalFile(file, "text/plain");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View("~/Views/Shared/Error.cshtml");
        }
    }
}
