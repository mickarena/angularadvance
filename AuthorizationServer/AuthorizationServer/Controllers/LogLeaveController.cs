﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthorizationServer.Models;
using AuthorizationServer.Repositories.Interfaces;
using AuthorizationServer.UnitOfWork;
using AuthorizationServer.ViewModels.Common;
using AuthorizationServer.ViewModels.LogLeave;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OpenIddict.Validation;

namespace AuthorizationServer.Controllers
{
    [Route("api/LogLeave")]
    [ApiController]
    public class LogLeaveController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;

        public LogLeaveController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpGet("Get")]
        public async Task<PagedListResponse<LogLeave>> GET(int pageIndex=0, int numberOfPage = 10000, string sortColumn ="", string sortType = "")
        {
            var user = await _userManager.GetUserAsync(User);
            var listLogLeave = _unitOfWork.LogLeaves.List().Where(p=> p.UserID == user.Id);

           switch (sortColumn)
           {
               case "FromDate":
                   listLogLeave = sortType == "asc" ? listLogLeave.OrderBy(p => p.FromDate) : listLogLeave.OrderByDescending(p => p.FromDate);
                   break;
               case "ToDate":
                   listLogLeave = sortType == "asc" ? listLogLeave.OrderBy(p => p.ToDate) : listLogLeave.OrderByDescending(p => p.ToDate);
                   break;
               default:
                   listLogLeave = listLogLeave.OrderByDescending(p => p.CreateDate);
                   break;
           }
            

            var resultData = listLogLeave
              .Skip(numberOfPage * pageIndex)
              .Take(numberOfPage).ToList();

            resultData = resultData.Select(p => { p.StatusName = p.Status == 1 ? "approved" : (p.Status == 2 ? "rejected" : "pending"); return p; }).ToList();

            return GenericResponse.SuccessPagedList(
               resultData,
               listLogLeave.Count()
           );
        }


        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpGet("GETAll")]
        public async Task<PagedListResponse<LogLeave>> GETAll(int pageIndex = 0, int numberOfPage = 10000, string sortColumn = "", string sortType = "")
        {
            // var user = await _userManager.GetUserAsync(User);
            var listLogLeave = _unitOfWork.LogLeaves.List();

            switch (sortColumn)
            {
                case "FromDate":
                    listLogLeave = sortType == "asc" ? listLogLeave.OrderBy(p => p.FromDate) : listLogLeave.OrderByDescending(p => p.FromDate);
                    break;
                case "ToDate":
                    listLogLeave = sortType == "asc" ? listLogLeave.OrderBy(p => p.ToDate) : listLogLeave.OrderByDescending(p => p.ToDate);
                    break;
                default:
                    listLogLeave = listLogLeave.OrderByDescending(p => p.CreateDate);
                    break;
            }


            var resultData = listLogLeave
              .Skip(numberOfPage * pageIndex)
              .Take(numberOfPage).ToList();


            resultData = resultData.Select(p => { p.StatusName = p.Status == 1 ? "approved" : (p.Status == 2 ? "rejected" : "pending"); return p; }).ToList();



            return GenericResponse.SuccessPagedList(
               resultData,
               listLogLeave.Count()
           );
        }

        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpPost("Create")]
        public async Task<IActionResult> Post([FromBody] LogLeaveViewModel item)
        {
            var user = await _userManager.GetUserAsync(User);

            var typeName = "";
            switch (item.TypeID)
            {
                case 1:
                    typeName = "sick leave";
                    break;
                case 2:
                    typeName = "take leave";
                    break;
                case 3:
                    typeName = "I like to rest";
                    break;
            }

            var model = new LogLeave() { 
             Approver = item.Approver,
             Comment = item.Comment,
             FromDate = item.FromDate,
             ToDate = item.ToDate,
             Reason = item.Reason,
             Status = item.Status,
             TypeID = item.TypeID,
             UserID = user.Id,
             CreateDate = DateTime.UtcNow,
             UserFullName = user.FullName,
             ApproverName = "Administrator HVN",
             TypeName = typeName,
             StatusName = "Pending"
            };
            var listLogLeave = _unitOfWork.LogLeaves.Add(model);
            _unitOfWork.SaveChanges();
            return Ok(GenericResponse.SuccessResult()); 
        }


        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpPut("Update")]
        public async Task<IActionResult> Put([FromBody] LogLeaveViewModel item)
        {
            var typeName = "";
            switch (item.TypeID)
            {
                case 1:
                    typeName = "sick leave";
                    break;
                case 2:
                    typeName = "take leave";
                    break;
                case 3:
                    typeName = "I like to rest";
                    break;
            }

            var user = await _userManager.GetUserAsync(User);
            var model = _unitOfWork.LogLeaves.List().Where(p => p.ID == item.ID).FirstOrDefault();
            model.Approver = item.Approver;
            model.Comment = item.Comment;
            model.FromDate = item.FromDate;
            model.ToDate = item.ToDate;
            model.Reason = item.Reason;
            model.Status = item.Status;
            model.TypeID = item.TypeID;
            model.UserID = user.Id;
            model.UserFullName = user.FullName;
            model.ApproverName = "Administrator HVN";
            model.TypeName = typeName;
            model.StatusName = "Pending";

            var listLogLeave = _unitOfWork.LogLeaves.Update(model);
            _unitOfWork.SaveChanges();
            return Ok(GenericResponse.SuccessResult());
        }

        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpGet("Delete")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await _userManager.GetUserAsync(User);

            var model = _unitOfWork.LogLeaves.List().Where(p=>p.ID == id).FirstOrDefault();
            _unitOfWork.LogLeaves.Remove(model);
            _unitOfWork.SaveChanges();
            return Ok(GenericResponse.SuccessResult());
        }


        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpPost("Approvel")]
        public async Task<IActionResult> Approvel([FromBody] ApprovelViewModel item)
        {
            var user = await _userManager.GetUserAsync(User);

            var model = _unitOfWork.LogLeaves.List().Where(p => p.ID == item.ID).FirstOrDefault();
            model.Status = item.Status;
            model.ApprovelComment = item.ApprovelComment;
            _unitOfWork.LogLeaves.Update(model);
            _unitOfWork.SaveChanges();
            return Ok(GenericResponse.SuccessResult());
        }

    }
}


