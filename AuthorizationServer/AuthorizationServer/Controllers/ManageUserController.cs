﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthorizationServer.Models;
using AuthorizationServer.Repositories.Interfaces;
using AuthorizationServer.UnitOfWork;
using AuthorizationServer.ViewModels.Account;
using AuthorizationServer.ViewModels.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OpenIddict.Validation;

namespace AuthorizationServer.Controllers
{
    [Route("api/ManageUser")]
    [ApiController]
    public class ManageUserController : Controller
    {
        private IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly string passWord_Default = "Abc@123";
        private readonly string phoneName_Default = "0989040438";

        public ManageUserController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpGet("GetUserAdmin")]
        public async Task<IActionResult> GetUserAdmin()
        {
            var admin = _userManager.Users.Where(p => p.UserName == "administrator_angular@gmail.com").FirstOrDefault();
            return Ok(admin);
        }

        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpGet("Get")]
        public async Task<PagedListResponse<ApplicationUser>> GET(int pageIndex = 0, int numberOfPage = 10000, string sortColumn = "", string sortType = "")
        {
            var user =  _userManager.Users.Where(p=>p.UserName != "administrator_angular@gmail.com");
          
            
            switch (sortColumn)
            {
                case "FullName":
                    user = sortType == "asc" ? user.OrderBy(p => p.FullName) : user.OrderByDescending(p => p.FullName);
                    break;
                case "UserName":
                    user = sortType == "asc" ? user.OrderBy(p => p.UserName) : user.OrderByDescending(p => p.UserName);
                    break;
                default:
                    user = user.OrderByDescending(p => p.CreateDate);
                    break;
            }
            

            var resultData = user
             .Skip(numberOfPage * pageIndex)
             .Take(numberOfPage).ToList();

            return GenericResponse.SuccessPagedList(
               resultData,
               user.Count()
           );
        }

        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpPost("Create")]
        public async Task<IActionResult> Post([FromBody] ManageViewModel model)
        {
            var isDublicate = _userManager.Users.Any(p => (!string.IsNullOrEmpty(p.Code) && p.Code.Trim().ToLower() == model.Code.Trim().ToLower())
                                                        || (!string.IsNullOrEmpty(p.UserName) && p.UserName.Trim().ToLower() == model.UserName.Trim().ToLower())
            );

            if (isDublicate)
            {
                return Ok(GenericResponse.FailureResultMesage(400, "Dublicate email or employee code"));
            }


            var user = new ApplicationUser
            {
                UserName = model.UserName,
                Email = model.UserName,
                FullName = model.FullName,
                PhoneNumber = !string.IsNullOrEmpty(model.PhoneNumber) ? model.PhoneNumber : phoneName_Default,
                Code = model.Code,
                CreateDate = DateTime.UtcNow,
                Birthday = model.Birthday
            };
            var result = await _userManager.CreateAsync(user, passWord_Default);
            return Ok(GenericResponse.SuccessResult());
        }


        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromBody] ManageViewModel model)
        {
            var isDublicate = _userManager.Users.Any(p => p.Id != model.Id &&
                                                        ( (!string.IsNullOrEmpty(p.Code) && p.Code.Trim().ToLower() == model.Code.Trim().ToLower())
                                                          || (!string.IsNullOrEmpty(p.UserName) && p.UserName.Trim().ToLower() == model.UserName.Trim().ToLower()) )
            );

            if (isDublicate)
            {
                return Ok(GenericResponse.FailureResultMesage(400, "Dublicate email or employee code"));
            }

            var user =  _userManager.Users.Where(p=>p.Id == model.Id).FirstOrDefault();
            user.UserName = model.UserName;
            user.Email = model.UserName;
            user.FullName = model.FullName;
            user.PhoneNumber = !string.IsNullOrEmpty(model.PhoneNumber) ? model.PhoneNumber : phoneName_Default;
            user.Code = model.Code;
            user.CreateDate = DateTime.UtcNow;
            user.Birthday = model.Birthday;

            var result = await _userManager.UpdateAsync(user);
            return Ok(GenericResponse.SuccessResult());
        }


        [Authorize(AuthenticationSchemes = OpenIddictValidationDefaults.AuthenticationScheme)]
        [HttpGet("Delete")]
        public async Task<IActionResult> Delete(string id)
        {
            var user = _userManager.Users.Where(p => p.Id == id).FirstOrDefault();
            var result = await _userManager.DeleteAsync(user);
            return Ok(GenericResponse.SuccessResult());
        }

    }
}
