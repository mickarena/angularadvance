﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthorizationServer.Models;
using AuthorizationServer.Repositories.Interfaces;
using AuthorizationServer.UnitOfWork;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationServer.Controllers
{
    public class ProviceController : Controller
    {

        private IUnitOfWork _unitOfWork;

        public ProviceController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        public List<Province> Index()
        {
            return _unitOfWork.Provinces.List().ToList();
        }
    }
}
