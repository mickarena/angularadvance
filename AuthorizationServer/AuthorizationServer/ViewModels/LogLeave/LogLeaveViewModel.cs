﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationServer.ViewModels.LogLeave
{
    public class LogLeaveViewModel
    {

        public int? ID { get; set; }


        public string UserID { get; set; }

    
        public string Approver { get; set; }

        public int? TypeID { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string Reason { get; set; }

        public string Comment { get; set; }

        public int? Status { get; set; }
    }
}
