﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationServer.ViewModels.LogLeave
{
    public class ApprovelViewModel
    {
        public int? ID { get; set; }
        public string ApprovelComment { get; set; }
        public int? Status { get; set; }
    }
}
