﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AuthorizationServer.ViewModels.Account
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [DisplayName("Họ tên")]
        [Required(ErrorMessage ="Bạn phải điền thông tin họ tên")]
        public string FullName { get; set; }
        [DisplayName("Số điện thoại")]
        [Required(ErrorMessage = "Bạn phải điền số điện thoại")]
        public string PhoneNumber { get; set; }
    }
}
