﻿using AuthorizationServer.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AuthorizationServer.ViewModels.Account
{
    public class UpdateViewModel
    {
        public UpdateViewModel()
        {
            ListProvice = new List<Province>();
        }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [DisplayName("Họ tên")]
        [Required(ErrorMessage = "Bạn phải điền thông tin họ tên.")]
        public string FullName { get; set; }
        [DisplayName("Số điện thoại")]
        [Required(ErrorMessage = "Bạn phải điền số điện thoại.")]
        public string PhoneNumber { get; set; }


        public List<Province> ListProvice { get; set; }
    }
}
