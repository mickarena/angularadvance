﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationServer.ViewModels.Common
{
    public class GenericResponse
    {
        public int Code { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        //public List<ValidationDetail> Errors { get; set; }
        public static GenericResponse SuccessResult()
        {
            return new GenericResponse
            {
                Success = true,
                Code = 0
            };
        }

        public static GenericResponse FailureResult(int? code = null)
        {
            return new GenericResponse
            {
                Success = false,
                Code = code.HasValue ? code.Value : 404
            };
        }

        public static GenericResponse FailureResultMesage(int? code = null, string msg = "")
        {
            return new GenericResponse
            {
                Message = msg,
                Success = false,
                Code = code.HasValue ? code.Value : 404
            };
        }

        public static GenericResponse<T> SuccessResult<T>(T item)
        {
            return new GenericResponse<T>
            {
                Success = true,
                Code = 0,
                Content = item
            };
        }
        public static GenericResponse<T> SuccessResult<T>()
        {
            return new GenericResponse<T>
            {
                Success = true,
                Code = 0
            };
        }

        public static GenericResponse<T> FailureResult<T>(int? code = null)
        {
            return new GenericResponse<T>
            {
                Success = false,
                Code = code.HasValue ? code.Value : 404
            };
        }
        public static GenericResponse<T> FailureResult<T>(string message)
        {
            return new GenericResponse<T>
            {
                Success = false,
                Message = message
            };
        }

        public static GenericListResponse<T> SuccessResultList<T>(IEnumerable<T> items)
        {
            return new GenericListResponse<T>
            {
                Success = true,
                Code = 0,
                Content = items
            };
        }

        public static GenericListResponse<T> FailureResultList<T>(int? code = null)
        {
            return new GenericListResponse<T>
            {
                Success = false,
                Code = code.HasValue ? code.Value : 404
            };
        }

        public static PagedListResponse<T> SuccessPagedList<T>(IEnumerable<T> items, long totalRecords)
        {
            return new PagedListResponse<T>
            {
                Content = items,
                TotalRecords = totalRecords,
                Code = 0,
                Success = true
            };
        }

        public static PagedListResponse<T> FailurePagedList<T>(int? code = null)
        {
            return new PagedListResponse<T>
            {
                TotalRecords = 0,
                Success = false,
                Code = code.HasValue ? code.Value : 404
            };
        }
    }

    public class GenericResponse<T> : GenericResponse
    {
        public T Content { get; set; }
    }

    public class GenericListResponse<T> : GenericResponse
    {
        public IEnumerable<T> Content { get; set; }
    }


    public class PagedListResponse<T> : GenericListResponse<T>
    {
        public long TotalRecords { get; set; }
    }
}
