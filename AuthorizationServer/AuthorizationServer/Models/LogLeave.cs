﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AuthorizationServer.Models
{
    [Table("LogLeave")]
    public class LogLeave
    {
        [Key]
        [Required]
        public int ID { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string UserID { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string UserFullName { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string Approver { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string ApproverName { get; set; }

        public int? TypeID { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string TypeName { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Reason { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string Comment { get; set; }
        public int? Status { get; set; }

        public string StatusName { get; set; }

        [Column(TypeName = "nvarchar(500)")]
        public string ApprovelComment { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}
