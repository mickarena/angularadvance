﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AuthorizationServer.Migrations
{
    public partial class AddMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApprovelComment",
                table: "LogLeave",
                type: "nvarchar(500)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovelComment",
                table: "LogLeave");
        }
    }
}
